#!/bin/bash

set -x

if [[ $@ == *"gunicorn"* || $@ == *"runserver"* ]]; then
    /usr/bin/wait-for-it.sh ${DB_HOST}:${DB_PORT}
    python manage.py migrate --fake-initial --noinput
    echo "
from django.contrib.auth.models import User
try:
	User.objects.create_superuser('argonavt', 'admin@example.com', 'argonavt')
except django.db.utils.IntegrityError:
	print('User 'argonavt' already exists')
    " | python manage.py shell
fi

exec "$@"