<?php
/**
 * Created by argonavt.
 * Date: 27/01/18
 * Time: 12:16 PM
 */

if (!isset($_GET['server_name'])){
    die;
}
define('IN_HLSTATS', true);

echo "updating server ip ... <br>\n";

$server_name = $_GET['server_name'];
$ip = gethostbyname($server_name);

if ($server_name == $ip){
    echo "failed <br>\n";
    die;
}

echo "$server_name = $ip <br>\n";

require('config.php');

// Load required files
require(INCLUDE_PATH . '/class_db.php');
require(INCLUDE_PATH . '/functions.php');

echo "updating server ip ... ";
$db = new DB_mysql(DB_ADDR, DB_USER, DB_PASS, DB_NAME, DB_PCONNECT);
$db->query("UPDATE adm_servers SET ip='$ip' WHERE address = '".mysql_escape_string($server_name)."'");
echo "ok";