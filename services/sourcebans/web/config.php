<?php
/**
 * config.php
 *
 * This file contains all of the configuration for the db
 * that will
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SteamFriends (www.SteamFriends.com)
 * @package SourceBans
 */
if(!defined('IN_SB')){echo 'You should not be here. Only follow links!';die();}

define('DB_HOST', getenv('DB_HOST'));   					// The host/ip to your SQL server
define('DB_USER', getenv('DB_USER'));						// The username to connect with
define('DB_PASS', getenv('DB_PASS'));						// The password
define('DB_NAME', getenv('DB_NAME'));  						// Database name
define('DB_PREFIX', 'sb');					// The table prefix for SourceBans
define('DB_PORT', '3306');							// The SQL port (Default: 3306)
define('DB_CHARSET', 'utf8mb4');                    // The Database charset (Default: utf8)
define('STEAMAPIKEY', '');				// Steam API Key for Shizz
define('SB_WP_URL', getenv('SITE_URL'));       				//URL of SourceBans Site
define('SB_EMAIL', '');

define('DEVELOPER_MODE', strtolower(getenv('DEBUG')) === 'true');			// Use if you want to show debugmessages

//define('SB_MEM', '128M'); 				// Override php memory limit, if isn't enough (Banlist is just a blank page)
?>